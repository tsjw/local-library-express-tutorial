const mongoose = require('mongoose')

const Schema = mongoose.Schema

let GenreSchema = new Schema(
  {
    name: { type: String, minLength: 3, maxlength: 100 }
  }
)

GenreSchema
.virtual('url')
.get(function(){ return '/catalog/genre/' + this._id})

module.exports = mongoose.model('Genre', GenreSchema)